<?php

class Search extends CI_Controller
{
    
    public function index($offset = 0)
    {

        $limit = 10;
    
        $data = array();
        
        //検索条件
        $data['lat'] = $this->input->get('lat');
        $data['lon'] = $this->input->get('lon');
        $data['dist'] = $this->input->get('dist');

        $this->load->model('shop_model');
        
        //店舗リスト取得
        $shopList = $this->shop_model->getList();
        
        $data['totalCount'] = $this->shop_model->getTotalCount();
        $data['more'] = $this->shop_model->existMore();
        
        //取得条件
        $query = $this->shop_model->getQuery();
        
        $data['query'] = $query;
        
        //地図表示
        $data['map'] = $this->parser->parse('components/map',$data,true);
        
        $data['shopList'] = $shopList;
        $this->parser->parse('search/index',$data);

    }
   
    public function more()
    {
        $data = array();
        
        //緯度経度
        $data['lat'] = $this->input->get('lat');
        $data['lon'] = $this->input->get('lon');

        $this->load->model('shop_model');
       
        $offset = $this->input->get('id') * 10; 
       
        //店舗リスト取得
        $shopList = $this->shop_model->getList($offset);
       
       
        $data['totalCount'] = $this->shop_model->getTotalCount();
        $data['more'] = $this->shop_model->existMore();
        
        $data['i'] = $this->input->get('id') + 1; 
        $data['shopList'] = $shopList;
        $this->parser->parse('search/more',$data);

    }


    public function map()
    {
        $this->load->model('shop_model');

        $shopList = $this->shop_model->getList();



        $output = array();
        foreach ($shopList as $key => $value) {
            $info = array();
            $info[] = $value['ShopName'];
            $info[] = $value['Latitude'];
            $info[] = $value['Longitude'];
            $info[] = 1;
            $output[] = $info;
        }

        $data['output'] = json_encode($output);
        $this->parser->parse('search/map',$data);
    }

        

   



}

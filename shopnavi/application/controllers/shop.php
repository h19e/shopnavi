<?php

class Shop extends CI_Controller
{
    
    public function map()
    {
        $data['lat'] = $this->input->get('lat');
        $data['lon'] = $this->input->get('lon');
        $data['id'] = $this->input->get('id');
        $this->parser->parse('shop/map',$data);
    }

    public function pickup()
    {

    
        $data = array();
        
        //検索条件
        $data['lat'] = $this->input->get('lat');
        $data['lon'] = $this->input->get('lon');

        $this->load->model('shop_model');
        
        //店舗リスト取得
        $shopList = $this->shop_model->getList(1,1);
        
        $data['totalCount'] = $this->shop_model->getTotalCount();
        $data['more'] = $this->shop_model->existMore();
        
        //取得条件
        $query = $this->shop_model->getQuery();
        
        $data['query'] = $query;
        
        $data['shopList'] = $shopList;
        $this->parser->parse('shop/pickup',$data);

    }

}

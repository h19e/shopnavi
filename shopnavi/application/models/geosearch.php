<?php

class Geosearch extends CI_Model
{

    const GEOSEARCH_API = "http://contents.search.olp.yahooapis.jp/OpenLocalPlatform/V1/contentsGeoCoder";

    const APPID = "APPID_APPID_APPID";



    private $response = array();

    private $query;

    public function __construct()
    {
        parent::__construct();
    }

    public function setQuery($query)
    {
        $this->query = rawurlencode($query);
    }

    
    public function getList($results = 20,$start = 1)
    {
        
        if ($this->query == "") {
            return array();
        }

        $curl = curl_init();
        $url = self::GEOSEARCH_API . 
                "?appid=" . self::APPID .
                "&query=" . $this->query .
                "&category=address" . 
                "&results=" . $results ;
                //"&start=" . $start;

        curl_setopt($curl,CURLOPT_URL, $url);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        $result = curl_exec($curl);
        curl_close($curl);
        
        $areaList = simplexml_load_string($result);
       
        $geoList = array();
        
        $i = 0;
        foreach ($areaList->Feature as $feature) {

            list($lon,$lat) = explode(",",$feature->Geometry->Coordinates);

            $geoList[] = array(
                "name" => $feature->Name,
                "lat" => $lat,
                "lon" => $lon
                );
        
        }
       
        return $geoList;

    }













}

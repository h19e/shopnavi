<?php

class Shop_model extends CI_Model
{

    const SEARCH_API = "http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/localSearch";
    
    const CID = "d8a23e9e64a4c817227ab09858bc1330";

    const APPID = "APPID_APPID_APPID";
    
    const DEFAULT_DIST = 3;

    const DEFAULT_RESULTS = 10;
    
    const DEFAULT_SORT = "dist";

    private $query = '';

    public function getList($offset = 0,$results = self::DEFAULT_RESULTS)
    {
        
        $curl = curl_init();
        
        $conditions = array();
        
        $param['dist'] = $this->input->get('dist'); 
        
        
        //エリア
        if ($this->input->get('lat') && $this->input->get('lon')) {
            $conditions[] = "lat=" . $this->input->get('lat');
            $conditions[] = "lon=" . $this->input->get('lon');
            if (is_numeric($param['dist'])) {
                $conditions[] = "dist=" . $param['dist'];
            } else {
                $conditions[] = "dist=" . self::DEFAULT_DIST;
            }
        }

        $conditions[] = "results=" . $results;
        $conditions[] = "start=" . $offset;

        $queryList = array();
        foreach ($conditions as $condition) {
            $queryList[] = htmlspecialchars($condition);
        }


        $queryList[] = "gc=0205001";
        $queryList[] = "sort=dist";
        $queryList[] = "cid=" . self::CID;
        $queryList[] = "appid=" . self::APPID;


        $this->query = implode("&",$queryList);


        $result = $this->request(self::SEARCH_API);



        $this->totalCount = (string)$result->ResultInfo->Total;
        $this->offset = $offset;
        
        $list = array();
        $i = 0;
        foreach ($result->Feature as $item) {
            $shop = array();
            $shop['ShopCode'] = (string)$item->Id;
            $shop['ShopName'] = (string)$item->Name;
            $shop['Address'] = (string)$item->Property->Address;
            list($lon,$lat) = explode(",",(string)$item->Geometry->Coordinates);
            $shop['Latitude'] = $lat;
            $shop['Longitude'] = $lon;
            $shop['StationName'] = (string)$item->Property->Station->Name;
            $list[$i] = $shop;
            $i++;
        }     

        return $list;

    }
    
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    public function existMore()
    {
        if ($this->totalCount > $this->offset + self::DEFAULT_RESULTS) {
            return true;
        } else {
            return false;
        }
    }


    
    private function request($url)
    {

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $url . "?" . $this->query);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($curl);
        curl_close($curl);
        
        return simplexml_load_string($response);
    }

    public function getQuery()
    {
        return $this->query;
    }


}


function location_search (targetUrl) {

    var gps = navigator.geolocation;
    var lat;
    var lon;
    
    if (gps) {
        gps.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            location.href= targetUrl + "lat=" + lat + "&lon=" + lon;
        },
        function(error) {
            alert('現在地が取得できませんでした');
        },
        {enableHighAccuracy:true,timeout:10000,maximumAge:0});
    } else {
        gps = google.gears.factory.create('beta.geolocation');
        gps.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            location.href= targetUrl + "lat=" + lat + "&lon=" + lon;
        },
        function(error) {
            alert('現在地が取得できませんでした');
        });
    }
}


function location_display() {
    var gps = navigator.geolocation;
    var lat;
    var lon;
    
    if (gps) {
        gps.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            
            $.getJSON("http://reverse.search.olp.yahooapis.jp/OpenLocalPlatform/V1/reverseGeoCoder?output=json&lat=" + lat + "&lon=" + lon + "&appid=APPID_APPID_APPID&callback=?",function(json){
                        
                address = json.Feature[0].Property.Address;
                $("#current_position").html("現在地 (" + address + ")");
            
            
                $("#near_shop").load("/shop/pickup/?lat=" + lat + "&lon=" + lon + " #parts",function(){
                    $("#near_shop").page();
                });
                
                var myLatlng = new google.maps.LatLng(lat,lon);
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
            });
    
        },
        function(error) {
            $("#current_position").html("現在地が取得できませんでした");
        },
        {enableHighAccuracy:true,timeout:10000,maximumAge:0});
    } else {
        gps = google.gears.factory.create('beta.geolocation');
        gps.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            $.getJSON("http://reverse.search.olp.yahooapis.jp/OpenLocalPlatform/V1/reverseGeoCoder?output=json&lat=" + lat + "&lon=" + lon + "&appid=APPID_APPID_APPID&callback=?",function(json){
                        
                address = json.Feature[0].Property.Address;
                $("#current_position").html("現在地 (" + address + ")");
                
                $("#near_shop").load("/shop/pickup/?lat=" + lat + "&lon=" + lon + " #parts",function(){
                    $("#near_shop").page();
                });
                
                var myLatlng = new google.maps.LatLng(lat,lon);
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
                
                
            });
        },
        function(error) {
            $("#current_position").html("現在地が取得できませんでした");
        });
    }
}
    



